# Belajar DevOps

[![Proses Devops](img/proses-devops.jpg)](img/proses-devops.jpg)

[![Skema Devops](img/skema-devops-diagram.png)](img/skema-devops-diagram.png)

[![Tools Devops](img/tools-devops.png)](img/tools-devops.png)

## Kebutuhan Software ##

* [Java SDK 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
* [Docker Desktop](https://www.docker.com/products/docker-desktop/)
* [Git](https://git-scm.com/downloads)
* [Visual Studio Code + Extension Pack for Java](https://code.visualstudio.com/)
