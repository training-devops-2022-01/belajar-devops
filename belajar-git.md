# Belajar Git #

1. Membuat SSH Keypair (pasangan public key dan private key)


    a. Buka `Git Bash`

    b. Jalankan perintah berikut di dalam git bash

        ssh-keygen
    
    c. Buka file `C:\Users\endy\.ssh\id_rsa.pub` dengan VSCode atau Notepad

    d. Copy dan paste di Gitlab.com > Preferences > SSH Keys

    [![Gitlab Preferences](img/gitlab-preferences.png)](img/gitlab-preferences.png)

    [![SSH Keys](img/gitlab-ssh-key.png)](img/gitlab-ssh-key.png)
    
    e. Clone repository yang sudah ada

        git clone <URL>
        
      Contohnya

        git clone git@gitlab.com:training-devops-2022-01/belajar-devops.git

2. Perintah-perintah standar git

    * Menambahkan perubahan ke staging area

        ```
        git add .
        git add -p .
        ```

    * Menyimpan perubahan di staging area ke local repo

        ```
        git commit -m "keterangan perubahan"
        ```

    * Mengupload local repo ke remote repo

        ```
        git push origin master
        git push -u origin master
        ```

    * Mendownload perubahan di remote repo ke local repo

        ```
        git pull
        git fetch
        git merge remotes/origin/master
        ```

3. Branching


    * Membuat branch

        ```
        git branch namabranch
        ```

    * Pindah ke branch yang baru

        ```
        git checkout namabranch
        ```
    
    * Commit ke branch tersebut

        - edit file
        - git add
        - git commit (pastikan ke branch yang barusan dibuat, bukan ke main/master)

    * Lihat daftar branch

        ```
        git branch -a
        ```

    * Pindah ke branch main/master

        ```
        git checkout master
        git checkout main
        ```
    
4. Merging

    * Ambil data semua branch yang ada di remote

        ```
        git fetch
        git fetch nama-remote
        ```

    * Cek ada branch apa saja

        ```
        git branch -a
        ```

    * Pindah ke branch tujuan

        ```
        git checkout branch-tujuan
        ```

    * Merge branch yang mau ditarik datanya

        ```
        git merge branch-asal
        ```

    * Resolve conflict
    * Cek isi file yang tadi diedit, bandingkan dengan isi file di branch yang baru

    * Push local branch ke remote repository

        ```
        git push namaremote namabranch
        git push -u namaremote namabranch
        ```


## Penjelasan ##

1. Lokasi penyimpanan dalam git

    [![Git Storage](img/git-storage.jpg)](img/git-storage.jpg)

2. Penanganan konflik

    [![Git Conflict](img/git-conflict.jpg)](img/git-conflict.jpg)

3. Branch & Merge

    [![Merge](img/git-merge.jpg)](img/git-merge.jpg)

4. Reset

    [![Git Reset](img/git-reset.jpg)](img/git-reset.jpg)

## Referensi ##

* [Git Workflow](https://docs.google.com/presentation/d/1kktaQTBgAkZPY-7R3ZrB5rR3JhqIjsFUnZyL0joTF_c/edit#slide=id.p)