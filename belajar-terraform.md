# Membuat VPS dengan Terraform #

## Pre-requisite ##

* Punya akun google cloud platform
* Membuat project di [Google Cloud Console](https://console.cloud.google.com/)
* [Instalasi Terraform](https://www.terraform.io/downloads) di laptop
* [Instalasi Google Cloud SDK](https://cloud.google.com/sdk/docs/install-sdk) di laptop

## Menjalankan Terraform ##

1. [Enable Google Compute Engine API](https://console.cloud.google.com/marketplace/product/google/compute.googleapis.com)
2. Buat script terraform, yaitu [main.tf](terraform/main.tf). Edit dan sesuaikan nama project di baris 2

    ```
    project = "training-devops-365802"
    ```

3. Login dulu ke gcloud

    ```
    gcloud auth application-default login 
    ```

4. Cek rencana eksekusi Terraform

    ```
    terraform plan
    ```

5. Jalankan terraform

    ```
    terraform apply
    ```

    Nanti akan ditanya apakah kita yakin ingin menjalankan file? Jawab `yes`

    ```
    Do you want to perform these actions?
    Terraform will perform the actions described above.
    Only 'yes' will be accepted to approve.

    Enter a value: yes
    ```

6. URL web yang baru dibuat bisa dilihat di outputnya

    ```
    Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

    Outputs:

    Web-server-URL = "http://34.101.103.153"
    ```