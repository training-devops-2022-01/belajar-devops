# Infrastructure as Code #

IaC Landscape :

1. Pet vs Cattle
2. Provisioner vs Configuration Management vs Package Management
3. Declarative vs Imperative

[![Infra as Code](img/infrastructure-as-code.jpg)]

## Daftar Tools ##

* Provisioner

    * Terraform
    * CloudFormation
    * Pulumi
    * Heat

* Configuration Management

    * Chef
    * Ansible
    * Puppet

* Runtime

    * Bare Metal
    * VPS
    * Kubernetes On Premise

        * Minikube
        * k3s
        * OKD
        * Rancher

    * Kubernetes as Service

        * Google Kubernetes Engine (GKE)
        * Amazon Elastic Kubernetes Service (EKS)
        * Microsoft Azure Kubernetes Service (AKS)
        * DigitalOcean Kubernetes Service (DoKS)

# Referensi #

* [IaC Landscape](https://blog.gruntwork.io/why-we-use-terraform-and-not-chef-puppet-ansible-saltstack-or-cloudformation-7989dad2865c)